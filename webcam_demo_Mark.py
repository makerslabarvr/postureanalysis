import tensorflow as tf
import cv2
import time
import argparse
import time
import csv

import posenet

parser = argparse.ArgumentParser()
parser.add_argument('--model', type=int, default=101)
parser.add_argument('--cam_id', type=int, default=0)
parser.add_argument('--cam_width', type=int, default=1280)
parser.add_argument('--cam_height', type=int, default=720)
parser.add_argument('--scale_factor', type=float, default=0.7125)
parser.add_argument('--file', type=str, default=None, help="Optionally use a video file instead of a live camera")
args = parser.parse_args()

fourcc = cv2.VideoWriter_fourcc('M','P','4','V')
out = cv2.VideoWriter('test.avi', fourcc, 4.0, (1280,720))

def main():
    with tf.Session() as sess:
        model_cfg, model_outputs = posenet.load_model(args.model, sess)
        output_stride = model_cfg['output_stride']

        if args.file is not None:
            cap = cv2.VideoCapture(args.file)
        else:
            cap = cv2.VideoCapture(args.cam_id)
        cap.set(3, args.cam_width)
        cap.set(4, args.cam_height)

        start = time.time()
        frame_count = 0
        f = open('pose.csv', 'a+')
        csv_writer = csv.writer(f, delimiter=',')
        while True:
            input_image, display_image, output_scale = posenet.read_cap(
                cap, scale_factor=args.scale_factor, output_stride=output_stride)

            heatmaps_result, offsets_result, displacement_fwd_result, displacement_bwd_result = sess.run(
                model_outputs,
                feed_dict={'image:0': input_image}
            )

            pose_scores, keypoint_scores, keypoint_coords = posenet.decode_multi.decode_multiple_poses(
                heatmaps_result.squeeze(axis=0),
                offsets_result.squeeze(axis=0),
                displacement_fwd_result.squeeze(axis=0),
                displacement_bwd_result.squeeze(axis=0),
                output_stride=output_stride,
                max_pose_detections=1,
                min_pose_score=0.15)

            keypoint_coords *= output_scale
            # print(type(keypoint_coords))
            # print(keypoint_coords.size)
            # print(keypoint_coords.shape)
            
            # check pose
            feedback_text = str()
            feedback_csv = list()
            feedback_csv.append(time.time())
            if keypoint_coords[0,9,1] < keypoint_coords[0,10,1]:
                print("arms crossed")
                print(keypoint_coords[0,9,1])
                print(keypoint_coords[0,10,1])
                feedback_text += "Arms crossed \n"
                feedback_csv.append("Arms crossed")
            else:
                print("arms not crossed")
                feedback_csv.append("Arms not crossed")
            
            if (keypoint_coords[0,[9,10],0] > keypoint_coords[0,[11,12],0]).any():
                print("arms too low")
                print(keypoint_coords[0,[9,10],0])
                print(keypoint_coords[0,[11,12],0])
                feedback_text += "Arms too low \n"
                feedback_csv.append("Arms too low")
            else:
                print("Arms not too low")
                feedback_csv.append("Arms not too low")
                
            if keypoint_coords[0,15,1] < keypoint_coords[0,16,1]:
                print("feet crossed")
                feedback_text += "Feet crossed \n"
                feedback_csv.append("Feet crossed")
            else:
                print("Feet not crossed")
                feedback_csv.append("Feet not crossed")
                
                
            print(feedback_text)

            # TODO this isn't particularly fast, use GL for drawing and display someday...
            overlay_image = posenet.draw_skel_and_kp(
                display_image, pose_scores, keypoint_scores, keypoint_coords,
                min_pose_score=0.15, min_part_score=0.1)

            cv2.putText(overlay_image, feedback_text,
                        (200,200),
                        cv2.FONT_HERSHEY_SIMPLEX,
                        1,
                        (0,255,255),
                        4)
            
            
            cv2.imshow('posenet', overlay_image)
            out.write(overlay_image)
            
            print(feedback_csv)
            csv_writer.writerow(feedback_csv)

            frame_count += 1
            if cv2.waitKey(1) & 0xFF == ord('q'):
                break

        print('Average FPS: ', frame_count / (time.time() - start))
        cap.release()
        out.release()
        f.close()


if __name__ == "__main__":
    main()